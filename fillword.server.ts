import Fastify from "fastify";
import fs from "fs";
import path from "path";
import logger from "./helpers/logger";
import { mainHandler } from "./fillword-server/main.handler";
import dotenv from "dotenv";
import { appendWords } from "./fillword-server/vocabulary";
import { resetSolving } from "./fillword-server/fillword.handler";

dotenv.config();
const fastify = Fastify({
  // logger: true,
});

fastify.listen({ port: 8080, host: "::" }, (err, address) => {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
  logger.info(`Сервер запущен http://localhost:8080`);
});

const scriptFilename = path.join(
  __dirname,
  "fillword-server",
  "scripts",
  "main.js"
);

fastify.get("/", (req, res) => {
  // console.log(req.headers);
  res.send({
    test: "new",
    script: fs.readFileSync(scriptFilename, "utf-8"),
  });
});

fastify.get("/img.html", (req, res) => {
  // @ts-ignore
  if ("word" in req.query) {
    const word = String(req.query.word);
    if (word) {
      logger.info(`Добавлено слово ${word}`);
      appendWords([word]);
      resetSolving();
    }
  }
  res.type("text/html").send(fs.readFileSync(path.join(__dirname, "img.html")));
});

fastify.get("/img.png", (req, res) => {
  // console.log(req.headers);
  res.send(fs.readFileSync(path.join(__dirname, "img.png")));
});

fastify.post("/", async (req, res) => {
  // @ts-ignore
  await mainHandler(req.body)
    .then((response) => {
      res.send(response);
    })
    .catch((err) => {
      console.error(err);
      res.send({});
    });
});
