import fs from "fs";
import path from "path";
import { fillwordHandler } from "./fillword.handler";
import logger from "../helpers/logger";

type Request = {
  /** Состояние приложения */
  state: any;
  /** Ссылка на страницу */
  referer: string;
};
type Response = {
  script?: string;
};
export async function mainHandler(req: Request): Promise<Response> {
  let script = "";
  // logger.info(`Referer: ${req.referer}`);
  if (req.referer.startsWith("https://yandex.ru/games/app/100048")) {
    logger.info("Кнопка старт");
    script = "start.button.js";
  }

  if (req.referer.includes("app-100048.games.s3.yandex.net")) {
    script = await fillwordHandler(req);
  }
  if (req.referer.includes("games-brotli.s3.yandex.net/100048")) {
    script = await fillwordHandler(req);
  }

  if (script && script.endsWith(".js")) {
    const scriptFilename = path.join(__dirname, "scripts", script);
    script = fs.readFileSync(scriptFilename, "utf-8");
  }
  if (script) {
    return { script };
  }
  return {};
}
