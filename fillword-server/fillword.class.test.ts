import { describe, expect, it } from "vitest";
import fs from "node:fs";
import path from "node:path";
import { Fillword } from "./fillword.class";

function getImage(name: string): Buffer {
  const filename = `${name}.png`;
  const fullFilename = path.join(__dirname, "images", "fillwords", filename);

  return fs.readFileSync(fullFilename);
}

describe("Fillword", () => {
  it("should recognize fillword", () => {
    const image = getImage("../fillword-15-clear");

    const fw = new Fillword(image);

    expect(fw.isRecognized).toBe(true);
  });

  describe("isSameFillword", () => {
    it("should detect same fillword with image", () => {
      const image = getImage("../fillword-15-clear");
      const imageWithImage = getImage("../fillword-15");

      const fw = new Fillword(image);
      const isSameFillword = fw.isSameFillword(imageWithImage);

      expect(isSameFillword).toBe(true);
    });

    it("should detect same fillword with solutions", () => {
      const image = getImage("../fillword-16-clear");
      const imageWithImage = getImage("../fillword-16");

      const fw = new Fillword(image);
      const isSameFillword = fw.isSameFillword(imageWithImage);

      expect(isSameFillword).toBe(true);
    });

    it("should detect not same fillword-1", () => {
      const image = getImage("../fillword-15-clear");
      const imageWithImage = getImage("../fillword-16");

      const fw = new Fillword(image);
      const isSameFillword = fw.isSameFillword(imageWithImage);

      expect(isSameFillword).toBe(false);
    });

    it("should detect not same fillword-2", () => {
      const image = getImage("АЖСПАРЕН");
      const imageWithImage = getImage("ТНЕЦИПЭА");

      const fw = new Fillword(image);
      const isSameFillword = fw.isSameFillword(imageWithImage);

      expect(isSameFillword).toBe(false);
    });

    it("should detect not same fillword, fully unmatched", () => {
      const image = getImage("../fillword-17-isSame-clear-fillword");
      const imageWithImage = getImage("../fillword-17-isSame-screenshot");

      const fw = new Fillword(image);
      const isSameFillword = fw.isSameFillword(imageWithImage);

      expect(isSameFillword).toBe(false);
    });
  });
});
