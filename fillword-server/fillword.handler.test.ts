import { describe, expect, it } from "vitest";
import { toMatchImageSnapshot } from "jest-image-snapshot";
import fs from "node:fs";
import path from "node:path";
import { analyzeFillword, drawSolution } from "./fillword.image";
import { solveFillword } from "./fillword.solver";

expect.extend({ toMatchImageSnapshot });

function getImage(name: string): Buffer {
  const filename = `${name}.png`;
  const fullFilename = path.join(__dirname, "images", filename);

  return fs.readFileSync(fullFilename);
}

describe("fillword.handler", () => {
  it("should solve fillword-1", async () => {
    const img = getImage("fillword-1");

    const contours = analyzeFillword(img);
    const solutions = solveFillword(contours);

    const sampleSolution = solutions.find((s) => s.word === "принц")!;

    const imageWithSolution = drawSolution(
      img,
      sampleSolution.solution,
      [0, 0, 255]
    );

    expect(imageWithSolution).toMatchImageSnapshot();
  });

  it("should solve fillword-2", async () => {
    const img = getImage("fillword-2");

    const contours = analyzeFillword(img);
    const solutions = solveFillword(contours);

    const sampleSolution = solutions.find((s) => s.word === "жакет")!;

    const imageWithSolution = drawSolution(
      img,
      sampleSolution.solution,
      [0, 0, 255]
    );

    expect(imageWithSolution).toMatchImageSnapshot();
  });
});
