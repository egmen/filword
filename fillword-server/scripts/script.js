// ==UserScript==
// @name        New script
// @namespace   Violentmonkey Scripts
// @match       *://*.games.s3.yandex.net/*
// @match       *://*.games-brotli.s3.yandex.net/*
// @match       *://yandex.ru/games/app/*
// @grant       GM_xmlhttpRequest
// @version     1.0
// @author      -
// @description 01.08.2024, 21:49:34
// ==/UserScript==

HTMLCanvasElement.prototype.getContext = (function (origFn) {
  return function (type, attribs) {
    attribs = attribs || {};
    attribs.preserveDrawingBuffer = true;
    return origFn.call(this, type, attribs);
  };
})(HTMLCanvasElement.prototype.getContext);

const API_HOST = "https://printer.egmen.ru/";
const CYCLE_TIMEOUT = 60000;

(function () {
  const state = {};

  let mainCycleTimeout = null;
  function finishCycleCallback(nextCycleTimeout = 100) {
    clearInterval(mainCycleTimeout);
    mainCycleTimeout = setTimeout(() => finishCycleCallback(), CYCLE_TIMEOUT);
    setTimeout(() => mainCycle(), nextCycleTimeout);
  }
  function stopCycle() {
    clearInterval(mainCycleTimeout);
  }

  function mainCycle() {
    GM_xmlhttpRequest({
      method: "POST",
      url: API_HOST,
      data: JSON.stringify({ state, referer: window.location.href }),
      headers: {
        "Content-Type": "application/json",
      },
      onload: (res) => {
        const json = JSON.parse(res.response);
        if (json.script) {
          eval(json.script);
        }
      },
    });
  }

  finishCycleCallback(0);
})();
