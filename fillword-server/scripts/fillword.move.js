const way = [];

let zizag = way.shift();

let mouseState = "up";
let lastWay = null;

function fireEvent(eventName) {
  const nextCoordinates = (() => {
    if (eventName === "mousedown") {
      mouseState = "down";
      return zizag.at(0);
    }
    if (eventName === "mousemove") {
      mouseState = "move";
      return zizag.shift();
    }
    if (eventName === "mouseup") {
      mouseState = "up";
      return lastWay;
    }
  })();
  lastWay = nextCoordinates;

  const [clientX, clientY] = nextCoordinates;

  canvas.dispatchEvent(new MouseEvent(eventName, { clientX, clientY }));
}

function moveNext() {
  if (zizag.length > 0) {
    if (mouseState === "up") {
      fireEvent("mousedown");
    }
    fireEvent("mousemove");
    setTimeout(() => moveNext(), 500);
    return;
  }

  if (mouseState !== "up") {
    fireEvent("mouseup");
    if (zizag.length === 0 && way.length > 0) {
      zizag = way.shift();
      setTimeout(() => moveNext(), 1500);
      return;
    }
  }

  console.log("finish move");
  finishCycleCallback(1500);
}

console.log("start move");

moveNext();

state.stage = "move";
