function ensureClick(selector) {
  const button = document.querySelector(selector);
  if (button) {
    console.log(`Нажали кнопку: ${selector}`);
    button.click();
  }
}

ensureClick("[data-fullscreen-element-name=close-btn]");
ensureClick("[data-testid=Promo-CloseButton-ButtonContent]");
ensureClick("span.close-button_type_adv-fullscreen");
ensureClick("span.close-button_type_popup");
ensureClick("div.play-button");

finishCycleCallback(10000);
