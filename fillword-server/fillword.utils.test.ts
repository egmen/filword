import { describe, expect, it } from "vitest";
import {
  isSiblingCell,
  generateUnsolvedFillword,
  getUnsolvedParts,
  getCell,
  filterSiblingCells,
  extractSiblingCells,
} from "./fillword.utils";

describe("fillword.utils", () => {
  it("should generate fillword", () => {
    const fillword = generateUnsolvedFillword("ОВЫД");

    expect(fillword[0]).toEqual({
      color: "dedee0",
      letter: "О",
      x: 1,
      y: 1,
    });
    expect(fillword[1]).toEqual({
      color: "dedee0",
      letter: "В",
      x: 2,
      y: 1,
    });
    expect(fillword[3]).toEqual({
      color: "dedee0",
      letter: "Д",
      x: 2,
      y: 2,
    });
  });

  it("should detect sibling cells", () => {
    const fillword = generateUnsolvedFillword("ОВЫВСДАКМ");

    expect(isSiblingCell(fillword[0], fillword[1])).toBe(true);
    expect(isSiblingCell(fillword[0], fillword[2])).toBe(false);
    expect(isSiblingCell(fillword[0], fillword[3])).toBe(true);
    expect(isSiblingCell(fillword[0], fillword[4])).toBe(false);
    expect(isSiblingCell(fillword[8], fillword[5])).toBe(true);
  });

  it("should filter sibling cells", () => {
    const fillword = generateUnsolvedFillword("О".repeat(9));

    const siblingCells = filterSiblingCells(fillword[0], fillword);

    expect(siblingCells).toEqual([fillword[1], fillword[3]]);
  });

  it("should extract sibling cells", () => {
    const fillword = generateUnsolvedFillword("О".repeat(9));

    const siblingCells = extractSiblingCells(fillword[0], fillword);

    expect(siblingCells).toHaveLength(2);
    expect(fillword).toHaveLength(7);
  });

  describe("unsolvedParts", () => {
    it("should no detect unsolved parts", () => {
      expect(getUnsolvedParts([])).toEqual([]);
    });

    it("should detect fully unsolved fillword", () => {
      const fillword = generateUnsolvedFillword("ОВЫД");

      const unsolvedParts = getUnsolvedParts(fillword);

      expect(unsolvedParts).toHaveLength(1);
      expect(unsolvedParts[0]).toHaveLength(4);
    });

    it("should detect one part unsolved", () => {
      const fillword = generateUnsolvedFillword("ОВЫД");
      fillword[0].color = "000000";
      fillword[1].color = "000000";

      const unsolvedParts = getUnsolvedParts(fillword);

      expect(unsolvedParts).toHaveLength(1);
      expect(unsolvedParts[0]).toHaveLength(2);
    });

    it("should detect two parts unsolved", () => {
      const fillword = generateUnsolvedFillword("ОВЫД");
      getCell(fillword, 1, 1)!.color = "000000";
      getCell(fillword, 2, 2)!.color = "000000";

      const unsolvedParts = getUnsolvedParts(fillword);

      expect(unsolvedParts).toHaveLength(2);
      expect(unsolvedParts).toEqual([
        [getCell(fillword, 1, 2)],
        [getCell(fillword, 2, 1)],
      ]);
    });

    it("should detect one part unsolved", () => {
      /**
       * -00
       * 0-0
       * 000
       */
      const fillword = generateUnsolvedFillword("О".repeat(9));
      getCell(fillword, 1, 1)!.color = "000000";
      getCell(fillword, 2, 2)!.color = "000000";

      const unsolvedParts = getUnsolvedParts(fillword);

      expect(unsolvedParts).toHaveLength(1);
    });
  });
});
