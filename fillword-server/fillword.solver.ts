import { getVocabulary } from "./vocabulary";
import {
  getUnsolvedParts,
  isSiblingCell,
  MINIMUM_UNSOLVED_PART_LENGTH,
} from "./fillword.utils";
import { FillwordCell } from "./fillword-cell.class";
import { computed, observable } from "mobx";

export class FillwordSolution {
  @observable public solution: FillwordCell[];
  constructor(solution: FillwordCell[]) {
    this.solution = solution;
  }

  @computed get word() {
    return this.solution.map((c) => c.letter.toLowerCase() || "·").join("");
  }

  @computed get hash() {
    return this.solution.map((c) => `${c.x}${c.y}`).join("");
  }
}

export class FillwordSolverClass {
  public word: string;
  public cells: FillwordCell[] = [];
  public attempts: FillwordCell[][] = [];

  private fillword: FillwordCell[];
  constructor(word: string, fillword: FillwordCell[]) {
    this.word = word.toUpperCase();
    this.fillword = fillword;
    this.solve();
  }

  private solve() {
    this.word.split("").forEach((letter, index) => {
      let newAttempts: FillwordCell[][] = [];
      if (index === 0) {
        const firstCells = this.fillword.filter((c) =>
          c.letters.includes(letter)
        );
        newAttempts = firstCells.map((firstCell) => [firstCell]);
      } else {
        this.attempts.forEach((attempt) => {
          const lastCell = attempt.at(-1)!;
          const nextMoves = this.fillword.filter(
            (c) =>
              c.letters.includes(letter) &&
              !attempt.includes(c) &&
              isSiblingCell(lastCell, c)
          );
          newAttempts.push(...nextMoves.map((move) => [...attempt, move]));
        });
      }
      this.attempts = newAttempts;
    });
  }

  get isSolved() {
    return this.attempts.length > 0;
  }

  get solutions() {
    return this.attempts.map((attempt) => new FillwordSolution(attempt));
  }
}

function solveVocabulary(
  fillword: FillwordCell[],
  vocabulary: string[]
): FillwordSolution[] {
  const fillwordSolve: FillwordSolution[] = [];
  vocabulary.forEach((word) => {
    const fw = new FillwordSolverClass(word, fillword);
    if (fw.isSolved) {
      fillwordSolve.push(...fw.solutions);
    }
  });

  return fillwordSolve;
}

export function solveFillword(fillword: FillwordCell[]) {
  const vocabulary = getVocabulary();
  return solveVocabulary(fillword, vocabulary);
}

/** Максимальная длина слова для брутфорса
 *
 * (чтобы не искать 2+ решения, оно гарантированно одно)
 **/
const MAXIMUM_BRUTFORCE_LENGTH = 9;

function brutForcePartition(partition: FillwordCell[]) {
  if (
    partition.length < MINIMUM_UNSOLVED_PART_LENGTH ||
    partition.length > MAXIMUM_BRUTFORCE_LENGTH
  ) {
    throw new Error(
      `Неподходящая длина для брутфорса (${partition.length} букв): ${partition
        .map((c) => c.letter)
        .join()}`
    );
  }

  let attempts: FillwordCell[][] = partition.map((c) => [c]);

  Array.from({ length: partition.length - 1 }, () => {
    let newAttempts: FillwordCell[][] = [];
    attempts.forEach((attempt) => {
      const lastCell = attempt.at(-1)!;
      const nextMoves = partition.filter(
        (cell) => !attempt.includes(cell) && isSiblingCell(lastCell, cell)
      );
      newAttempts.push(...nextMoves.map((move) => [...attempt, move]));
    });
    attempts = newAttempts;
  });

  return attempts
    .filter((attempt) => attempt.length === partition.length)
    .map((attempt) => new FillwordSolution(attempt));
}

export function brutForceFillword(fillword: FillwordCell[]) {
  const partitions = getUnsolvedParts(fillword);
  const solutions: FillwordSolution[] = [];
  partitions.forEach((partition) => {
    solutions.push(...brutForcePartition(partition));
  });
  return solutions;
}
