import logger from "../helpers/logger";
import fs from "node:fs";
import path from "node:path";

const vocabularyFilename = path.join(__dirname, "vocabulary.txt");
const newVocabularyFilename = path.join(__dirname, "vocabulary.new.txt");
const brutForceVocabularyFilename = path.join(
  __dirname,
  "vocabulary.brutforce.txt"
);

const vocabulary: string[] = [];

function loadVocabulary() {
  vocabulary.length = 0;
  fs.readFileSync(vocabularyFilename, "utf8")
    .split("\n")
    .forEach((line) => {
      vocabulary.push(line);
    });
  fs.readFileSync(newVocabularyFilename, "utf8")
    .split("\n")
    .forEach((line) => {
      vocabulary.push(line);
    });
}

function watchVocabularyChanges() {
  fs.watchFile(newVocabularyFilename, () => {
    logger.info("Reloading vocabulary");
    loadVocabulary();
  });
}

let isLoaded = false;
function ensureLoaded() {
  if (isLoaded) {
    return;
  }
  loadVocabulary();
  watchVocabularyChanges();
  isLoaded = true;
}

export function getVocabulary() {
  ensureLoaded();
  return vocabulary;
}

export function appendWords(words: string[]) {
  ensureLoaded();
  const newWords = words
    .map((word) => word.toLowerCase())
    .filter((word) => word.length >= 3)
    .filter((word) => !vocabulary.includes(word));

  if (newWords.length > 0) {
    fs.appendFileSync(newVocabularyFilename, newWords.join("\n") + "\n");
  }
  vocabulary.push(...newWords);
}

export function addBrutForceWord(word: string) {
  fs.appendFileSync(brutForceVocabularyFilename, word + "\n");
}
