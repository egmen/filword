import path from "path";
import cv, { Contour, Mat, Point2, Rect, Vec3 } from "@u4/opencv4nodejs";
import {
  getPositiveBinary,
  recognizeLetterByTemplate,
  toBinaryImage,
  toBinaryImageColorLetter,
} from "./letters/recognize";
import { countBy, mapValues, maxBy, mean, pickBy } from "lodash";
import { FillwordMeta, ProtoCell } from "./fillword.types";
import crypto from "node:crypto";
import { FillwordCell } from "./fillword-cell.class";

const imagesCache = new Map<string, Mat>();

function ensureImage(name: string) {
  if (imagesCache.has(name)) {
    return imagesCache.get(name)!;
  }

  const imageFilename = path.join(__dirname, "images", `${name}.png`);
  const image = cv.imread(imageFilename);
  imagesCache.set(name, image);
  return image;
}

export function isButton(img: Buffer, name: string) {
  const image = cv.imdecode(img);

  const result = image.matchTemplate(
    ensureImage(`button.${name}`),
    cv.TM_CCOEFF_NORMED
  );
  const { maxVal, maxLoc } = result.minMaxLoc();
  const threshold = 0.8; // Пороговое значение
  if (maxVal >= threshold) {
    return maxLoc;
  } else {
    return false;
  }
}

export function isScreen(img: Buffer, name: string) {
  const image = cv.imdecode(img);

  const result = image.matchTemplate(
    ensureImage(`screen.${name}`),
    cv.TM_CCOEFF_NORMED
  );
  const { maxVal, maxLoc } = result.minMaxLoc();
  const threshold = 0.8; // Пороговое значение
  if (maxVal >= threshold) {
    return true;
  } else {
    return false;
  }
}

export function findCountours(image: Mat) {
  // Применение морфологических операций для объединения близких букв
  const kernel = cv.getStructuringElement(cv.MORPH_RECT, new cv.Size(10, 10)); // Размер ядра можно подбирать
  const dilatedImage = image.dilate(kernel);

  // Поиск контуров
  const contours = dilatedImage.findContours(
    cv.RETR_EXTERNAL,
    cv.CHAIN_APPROX_SIMPLE
  );

  return contours;
}

export function detectContours(binaryImage: Mat): Contour[] {
  return (
    findCountours(binaryImage)
      // Сортируем буквы
      .sort((c1, c2) => {
        const br1 = c1.boundingRect();
        const br2 = c2.boundingRect();
        if (isNextRow(br1, br2)) {
          return -1;
        }
        if (br1.x < br2.x) {
          return -1;
        }
        return 0;
      })
  );
}

function isNextRow(upperCell: Rect, bottomCell: Rect) {
  const bottomCellCalculatedY = upperCell.y + upperCell.height / 2;
  return bottomCell.y > bottomCellCalculatedY;
}

export function filterBottomRows(cells: ProtoCell[]) {
  let lastCell: ProtoCell;
  let avgRowHeight: number;
  let isLastRow = false;
  return cells
    .filter((cell) => {
      const cellSquare = cell.letterContour.width * cell.letterContour.height;
      return cellSquare > 500;
    })
    .filter((cell) => {
      if (isLastRow) {
        return false;
      }
      // Определяем, что ряд изменился, для расчёта примерной высоты ряда
      if (
        !avgRowHeight &&
        lastCell &&
        isNextRow(lastCell.letterContour, cell.letterContour)
      ) {
        avgRowHeight = cell.letterContour.y - lastCell.letterContour.y;
      }

      // Отсекаем последний ряд
      if (avgRowHeight && lastCell) {
        /** Граница, гарантирующая, что ряд последний */
        const fillwordBorderY =
          lastCell.letterContour.y +
          avgRowHeight +
          lastCell.letterContour.height / 2;
        if (cell.letterContour.y > fillwordBorderY) {
          isLastRow = true;
          return false;
        }
      }

      lastCell = cell;
      return true;
    });
}

export function isBlackImage(image: Mat) {
  const arr = image.getDataAsArray().flat(1);
  const stat = countBy(arr);
  const blankPixels = stat[0] || 0;
  const whitePixels = stat[255] || 0;
  const whitePercent = (whitePixels / blankPixels) * 100;

  return whitePercent < 2;
}

export function calcColors(image: Mat) {
  const pixels = image
    .getDataAsArray()
    .flat(1)
    // @ts-expect-error
    .map(([b, g, r]) => `${r.toString(16)}${g.toString(16)}${b.toString(16)}`);
  const stat = countBy(pixels);
  const filteredStat = pickBy(stat, (s) => s > 100);
  const percent = mapValues(
    filteredStat,
    (cnt) => Math.ceil((cnt / pixels.length) * 1000) / 10
  );

  // @ts-expect-error
  const [color] = maxBy(Object.entries(percent), (val) => val[1]);
  const isStart = Object.keys(percent).length > 2;
  return {
    color,
    isStart,
  };
}

function splitAndCalcAvg(coord: number[]): number[] {
  const MIN_SIZE = 10;
  const groups: number[][] = [];
  let lastValue = -5;
  coord
    .sort((a, b) => a - b)
    .forEach((c) => {
      if (c > lastValue + MIN_SIZE) {
        groups.push([]);
      }
      groups.at(-1)?.push(c);
      lastValue = c;
    });
  return groups.map((group) => Math.round(mean(group)));
}

export function calcFillwordMeta(cells: ProtoCell[]): FillwordMeta {
  const yRow = cells.filter((cell) => !"ДЁЙЦЩ".includes(cell.letter));
  const allCenterY = yRow.map(
    (cell) => cell.letterContour.y + cell.letterContour.height / 2
  );

  const allCenterX = yRow.map(
    (cell) => cell.letterContour.x + cell.letterContour.width / 2
  );

  const xCenters = splitAndCalcAvg(allCenterX);

  return {
    yCenters: splitAndCalcAvg(allCenterY),
    xCenters,
    cellSize: (xCenters.at(-1)! - xCenters.at(0)!) / (xCenters.length - 1),
  };
}

function isInRect(p: Point2, rect: Rect) {
  return (
    p.x > rect.x &&
    p.x < rect.x + rect.width &&
    p.y > rect.y &&
    p.y < rect.y + rect.height
  );
}

export function calcFullFillword(image: Mat, cells: ProtoCell[]) {
  const meta = calcFillwordMeta(cells);
  const cellSize = meta.cellSize * 0.9;

  const fullCells: FillwordCell[] = [];
  meta.yCenters.map((y, yIndex) => {
    meta.xCenters.map((x, xIndex) => {
      const center = new Point2(x, y);
      const xFullCell = Math.round(x - cellSize / 2);
      const yFullCell = Math.round(y - cellSize / 2);
      const cellContour = new Rect(
        xFullCell,
        yFullCell,
        Math.round(cellSize),
        Math.round(cellSize)
      );
      const cellImage = image.getRegion(cellContour);
      const colors = calcColors(cellImage);

      const cell = new FillwordCell(
        xIndex + 1,
        yIndex + 1,
        colors.color,
        cellImage,
        cellContour,
        center,
        colors.isStart
      );

      fullCells.push(cell);
    });
  });
  return fullCells;
}

export function recognizeContours(
  image: Mat,
  contours: Contour[],
  originalImage: Mat
) {
  return contours.map((contour) => {
    const boundingRect = contour.boundingRect();
    const letterImage = image.getRegion(boundingRect);
    const letter = recognizeLetterByTemplate(
      letterImage,
      originalImage.getRegion(contour.boundingRect())
    );

    const cell: ProtoCell = {
      letterContour: boundingRect,
      letter,
    };
    return cell;
  });
}

export function mergeImages(
  mainImage: Mat,
  coloredImage: Mat,
  cells: FillwordCell[]
) {
  let resultImage = mainImage;
  cells.forEach((cell) => {
    const mainImageCell = mainImage.getRegion(cell.cellContour);
    if (isBlackImage(mainImageCell)) {
      const shrinkSize = 5;
      const shrinkedCell = new Rect(
        cell.cellContour.x + shrinkSize,
        cell.cellContour.y + shrinkSize,
        cell.cellContour.width - shrinkSize * 2,
        cell.cellContour.height - shrinkSize * 2
      );
      coloredImage
        .getRegion(shrinkedCell)
        .copyTo(resultImage.getRegion(shrinkedCell));
    }
  });
  return resultImage;
}

export function analyzeFillword(img: Buffer) {
  const image = cv.imdecode(img);

  /** Основное изображение для вычисления метаданных */
  const invertedBinaryImage = toBinaryImage(image);
  const positiveMainImage = getPositiveBinary(invertedBinaryImage);

  /** Дополнительное изображение для нераспознанных символов */
  const invertedColoredImage = toBinaryImageColorLetter(image);

  /** Вычисление контуров */
  const contours = detectContours(invertedBinaryImage);
  /** Протоячейки */
  const protoCellsAll = recognizeContours(positiveMainImage, contours, image);
  /** Фильрация протоячеек */
  const protoCells = filterBottomRows(protoCellsAll);
  if (false) {
    protoCells.forEach((cell, idx) => {
      // Рисуем контуры на оригинальном изображении
      image.drawRectangle(cell.letterContour, new cv.Vec3(0, 0, 255), 3);
      image.putText(
        String(idx),
        new cv.Point2(cell.letterContour.x, cell.letterContour.y - 6),
        1,
        1,
        { color: new cv.Vec3(0, 0, 255), thickness: 2 }
      );
    });
    cv.imwrite("img.png", image);
  }
  /** Построение полного филворда на основе метаданных */
  const fullFillword = calcFullFillword(image, protoCells);

  /** Объединение изображений на основе полного филворда */
  const mergedImage = mergeImages(
    invertedBinaryImage,
    invertedColoredImage,
    fullFillword
  );

  const positiveBinary = getPositiveBinary(mergedImage);
  const newContours = detectContours(mergedImage);
  const newProtoCells = recognizeContours(positiveBinary, newContours, image);

  // Считываем новые контуры
  fullFillword.forEach((cell) => {
    const letterContour = newProtoCells.find((c) =>
      isInRect(cell.center, c.letterContour)
    );
    cell.letterContour = letterContour?.letterContour;
    cell.letter = letterContour?.letter || "";
    // console.log();
    // Рисуем контуры на оригинальном изображении
    // if (DEBUG) {
    // positiveBinary.drawRectangle(
    //   cell.letterContour!,
    //   cell.isStart ? new cv.Vec3(0, 0, 255) : new cv.Vec3(0, 255, 0),
    //   cell.isStart ? 3 : 2
    // );
    // positiveBinary.putText(
    //   cell.letter,
    //   new cv.Point2(cell.letterContour.x, cell.letterContour.y + 7),
    //   3,
    //   1,
    //   { color: new cv.Vec3(0, 0, 255), thickness: 2 }
    // );
    // }
  });
  return fullFillword;
}

export function compareWithClearFillword(
  img: Buffer,
  clearImg: Buffer,
  clearSolution: FillwordCell[]
) {
  const dirtyImage = cv.imdecode(img);
  const clearImage = cv.imdecode(clearImg);

  const clearedFillword = clearSolution.map((cell) => {
    const colorCell = dirtyImage.getRegion(cell.cellContour);
    const colors = calcColors(colorCell);

    if (!cell.letterContour) {
      return cell.clone().setColor(colors.color);
    }
    const dirtyCell = dirtyImage.getRegion(cell.letterContour!);
    const clearCell = clearImage.getRegion(cell.letterContour!);

    const result = dirtyCell.matchTemplate(clearCell, cv.TM_CCOEFF_NORMED);
    const { maxVal } = result.minMaxLoc();
    const threshold = 0.8;
    if (maxVal >= threshold) {
      return cell.clone().setColor(colors.color);
    }

    return cell.clone().setColor("wrong");
  });

  return clearedFillword;
}

export function compareFillword(img: Buffer, clearImg?: Buffer) {
  let solution: FillwordCell[] = [];
  try {
    solution = analyzeFillword(img);
  } catch (err) {
    if (clearImg) {
      const clearSolution = analyzeFillword(clearImg);
      const dirtyImage = cv.imdecode(img);
      const clearImage = cv.imdecode(clearImg);
      let isAnySolvedLetter = false;
      const clearedFillword = clearSolution.map((cell) => {
        const dirtyCell = dirtyImage.getRegion(cell.letterContour!);
        const clearCell = clearImage.getRegion(cell.letterContour!);

        const result = dirtyCell.matchTemplate(clearCell, cv.TM_CCOEFF_NORMED);
        const { maxVal } = result.minMaxLoc();
        const threshold = 0.8;
        if (maxVal >= threshold) {
          isAnySolvedLetter = true;
          return cell;
        }
        cell.color = "wrong";
        return cell;
      });
      if (isAnySolvedLetter) {
        return clearedFillword;
      }
    }
    throw err;
  }
  return solution;
}

function md5(str: string): string {
  return crypto
    .createHash("md5")
    .update(Buffer.from(str, "utf-8"))
    .digest("hex");
}

function strToRgb(str: string): [number, number, number] {
  const bigint = parseInt(md5(str).slice(0, 6), 16);
  return [(bigint >> 16) & 255, (bigint >> 8) & 255, bigint & 255];
}

export function drawSolution(
  img: Buffer,
  solution: FillwordCell[],
  color?: [number, number, number]
): Buffer {
  const image = cv.imdecode(img);

  const solutionColor = new Vec3(
    ...(color ? color : strToRgb(solution.map((c) => c.letter).join("")))
  );

  let prevCell: Point2;
  solution.forEach((cell) => {
    const cc = cell.cellContour!;
    const currentCell = new Point2(cc.x + cc?.width / 2, cc.y + cc.height / 2);
    if (!prevCell) {
      image.drawCircle(currentCell, 4, solutionColor, 10);
    } else {
      image.drawLine(prevCell, currentCell, solutionColor, 3);
    }
    if (cell === solution.at(-1)) {
      image.drawCircle(currentCell, 4, solutionColor, 4);
    }
    prevCell = currentCell;
  });
  return cv.imencode(".png", image);
}
