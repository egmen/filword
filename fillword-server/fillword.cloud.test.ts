import fs from "node:fs";
import path from "node:path";
import { describe, expect, it } from "vitest";
import {
  AnagramType,
  parsePoncyResult,
  resolveAnagram,
} from "./fillword.cloud";

function loadFixture(name: string) {
  const filename = path.join(__dirname, "fixtures", name);

  return fs.readFileSync(filename, "utf-8");
}

describe("fillword cloud", () => {
  describe("parse results", () => {
    it("should parse html with one result", async () => {
      const result = parsePoncyResult(loadFixture("poncy.ru-1.html"));

      expect(result).toEqual(["ФУТБОЛКА"]);
    });

    it("should parse html with no result", async () => {
      const result = parsePoncyResult(loadFixture("poncy.ru-2.html"));

      expect(result).toEqual([]);
    });

    it("should parse html with many result", async () => {
      const result = parsePoncyResult(loadFixture("poncy.ru-3.html"));

      expect(result).toEqual(["АПЕЛЬСИН", "СПАНИЕЛЬ"]);
    });
  });

  describe("resolve anagrams", () => {
    it("should resolve anagram with one result", async () => {
      const result = await resolveAnagram("коутлбаф", AnagramType.Solve);

      expect(result).toEqual(["ФУТБОЛКА"]);
    });

    it("should resolve anagram with no result", async () => {
      const result = await resolveAnagram("коутлба", AnagramType.Solve);

      expect(result).toEqual([]);
    });

    it("should resolve anagram with multiple result", async () => {
      const result = await resolveAnagram("енаилпсь", AnagramType.Solve);

      expect(result).toEqual(["АПЕЛЬСИН", "СПАНИЕЛЬ"]);
    });
  });
});
