import { describe, expect, it } from "vitest";
import { FillwordCell } from "./fillword-cell.class";

function createCell(color = "000000") {
  // @ts-expect-error
  const cell = new FillwordCell(0, 0, color);
  return cell;
}

describe("FillwordCell", () => {
  it("should create", () => {
    const cell = createCell();

    expect(cell).toBeDefined();
  });

  describe("solved cell", () => {
    it("should detect solved cell", () => {
      const cell = createCell("000000");

      expect(cell.isSolved).toBe(true);
      expect(cell.isUnsolved).toBe(false);
    });

    it("should detect unsolved cell", () => {
      const cell = createCell("dedee0");

      expect(cell.isSolved).toBe(false);
      expect(cell.isUnsolved).toBe(true);
    });

    it("should change solved status", () => {
      const cell = createCell("dedee0");

      expect(cell.isSolved).toBe(false);

      cell.color = "000000";

      expect(cell.isSolved).toBe(true);
    });
  });

  describe.only("letter detect", () => {
    it.each([
      ["Ш", ["Ш", "Щ"]],
      ["Щ", ["Ш", "Щ"]],
      ["Ь", ["Ь", "Ъ"]],
      ["Ъ", ["Ь", "Ъ"]],
      ["Е", ["Е", "Ё"]],
      ["Ё", ["Е", "Ё"]],
    ])("should multiply %s", (letter, letters) => {
      const cell = createCell("dedee0");

      cell.letter = letter;

      expect(cell.letters).toEqual(letters);
    });

    it("should not multiply letter Ч", () => {
      const cell = createCell("dedee0");

      cell.letter = "Ч";

      expect(cell.letters).toEqual(["Ч"]);
    });
  });
});
