import { observable } from "mobx";
import { analyzeFillword, compareWithClearFillword } from "./fillword.image";
import { FillwordCell } from "./fillword-cell.class";
import {
  canBeSolved,
  getCell,
  getUnsolvedCells,
  isUnsolved,
} from "./fillword.utils";
import {
  brutForceFillword,
  FillwordSolution,
  solveFillword,
} from "./fillword.solver";
import { resolveAnagram } from "./fillword.cloud";
import logger from "../helpers/logger";

const MINIMAL_WORD_LENGTH = 3;
const MINIMAL_SOLVED_WORD_LENGTH = 5;

export class Fillword {
  @observable private cells: FillwordCell[] = [];

  @observable private newCells: FillwordCell[] = [];

  private newImage: Buffer | null = null;

  public isSolved = false;

  public isSolvedOnline = false;

  public isSolvedBrutForce = false;

  public solutions: FillwordSolution[] = [];

  public usedSolutions = new Set<FillwordSolution>();

  public lastSolution: FillwordSolution | undefined;

  get usedWords() {
    return new Set([...this.usedSolutions].map((s) => s.word));
  }

  constructor(private image: Buffer) {
    this.recognize();
    this.solve();
  }

  get isRecognized() {
    return this.cells.length > 0;
  }

  /** Филлворд решён полностью */
  public get isFullySolved() {
    return getUnsolvedCells(this.cells).length === 0;
  }

  /** Филлворд чистый, ещё не решён */
  public get isFullyUnsolved() {
    return isUnsolved(this.cells) && this.cells.length === 64;
  }

  /** Распознавание филлворда */
  private recognize() {
    if (!this.isRecognized) {
      this.cells = analyzeFillword(this.image);
    }
  }

  public async solveOnline() {
    if (!this.isSolvedOnline) {
      const unsolvedLetters = getUnsolvedCells(this.cells)
        .map((c) => c.letter)
        .join("");
      logger.info(`Подбор решения онлайн по буквам: ${unsolvedLetters}`);
      const words = await resolveAnagram(unsolvedLetters);
      logger.info(`Найдены слова: ${words.join(",")}`);
      this.solutions = solveFillword(this.cells);
      this.isSolvedOnline = true;
    }
  }

  public solveBrutForce() {
    if (!this.isSolvedBrutForce) {
      this.solutions = brutForceFillword(this.cells);
      this.isSolvedBrutForce = true;
    }
  }

  private solve() {
    if (!this.isSolved) {
      this.solutions = solveFillword(this.cells);
      this.isSolved = true;
    }
  }

  public isSameFillword(newImage: Buffer): boolean {
    this.newImage = newImage;
    this.newCells = compareWithClearFillword(newImage, this.image, this.cells);

    const isCanBeSolved = canBeSolved(this.newCells);
    if (isCanBeSolved) {
      this.cells.forEach((c) => {
        const newCell = getCell(this.newCells, c.x, c.y);
        if (newCell) {
          c.color = newCell.color;
        }
      });
    }

    return isCanBeSolved;
  }

  public get sortedSolutions(): FillwordSolution[] {
    return this.solutions
      .filter((s) => s.word.length >= MINIMAL_WORD_LENGTH)
      .sort((s1, s2) => {
        return s1.word.length - s2.word.length;
      });
  }

  public get sortedWords(): string[] {
    return Array.from(new Set(this.sortedSolutions.map((s) => s.word)));
  }

  private get unsolvedSolutions(): FillwordSolution[] {
    return this.sortedSolutions.filter((s) =>
      s.solution.every((c) => c.isUnsolved)
    );
  }

  private get unusedSolution() {
    return this.unsolvedSolutions
      .filter((s) => this.usedSolutions.has(s) === false)
      .filter((s) => {
        if (s.word.length >= MINIMAL_SOLVED_WORD_LENGTH) {
          return true;
        }
        return this.usedWords.has(s.word) === false;
      });
  }

  public getNextSolution(): FillwordSolution[] {
    if (!this.isSolved) {
      this.solve();
    }
    const smallSolutionLimit = 6;
    const smallSolutionList = this.unusedSolution.filter(
      (s) => s.word.length <= 4
    );

    const sampleSolutions =
      smallSolutionList.length > 0
        ? smallSolutionList.slice(0, smallSolutionLimit)
        : this.unusedSolution.slice(0, 1);

    sampleSolutions.forEach((s) => {
      this.usedSolutions.add(s);
    });

    if (sampleSolutions.length > 0) {
      this.lastSolution = sampleSolutions.at(0);
    }

    return sampleSolutions;
  }
}
