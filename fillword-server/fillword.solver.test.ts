import { describe, expect, it } from "vitest";
import {
  brutForceFillword,
  FillwordSolverClass,
  solveFillword,
} from "./fillword.solver";
import { generateUnsolvedFillword } from "./fillword.utils";

describe("fillword.solver", () => {
  it("should check solve fillword-1", () => {
    const fillword = generateUnsolvedFillword("ОВЫВСДАКМУПОЖАКРРБОДИНЦДТ");

    expect(new FillwordSolverClass("одав", fillword).isSolved).toBe(true);
    expect(
      new FillwordSolverClass("одав", fillword).attempts[0].map(
        ({ color, ...rest }) => rest
      )
    ).toEqual([
      {
        letter: "О",
        x: 1,
        y: 1,
      },
      {
        letter: "Д",
        x: 1,
        y: 2,
      },
      {
        letter: "А",
        x: 2,
        y: 2,
      },
      {
        letter: "В",
        x: 2,
        y: 1,
      },
    ]);
    expect(new FillwordSolverClass("овык", fillword).isSolved).toBe(true);
    expect(new FillwordSolverClass("овъ", fillword).isSolved).toBe(false);
    expect(new FillwordSolverClass("ОВЫВСУМКАДПОЖАК", fillword).isSolved).toBe(
      true
    );
  });

  it("should check solve fillword-2", () => {
    const fillword = generateUnsolvedFillword("ОООО");

    const result = new FillwordSolverClass("ОО", fillword);

    expect(result.isSolved).toBe(true);
    expect(result.attempts).toHaveLength(8);
  });

  it("should solve fillword-1", () => {
    const fillword = generateUnsolvedFillword("ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ");
    fillword.forEach((c, idx) => {
      if (idx <= 9) {
        c.color = "black";
      }
    });

    const result = solveFillword(fillword);

    expect(result.map((s) => s.word).length).toBeGreaterThan(30);
  });

  it("should check solve fillword-3", () => {
    const fillword = generateUnsolvedFillword("КОТОНЁКВЫ");

    const result = new FillwordSolverClass("котенок", fillword);

    expect(result.isSolved).toBe(true);
    expect(result.attempts).toHaveLength(1);
  });

  describe("brutforce solution", () => {
    it("should fails to generate solution (maximum)", () => {
      const fillword = generateUnsolvedFillword("ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ");

      const solve = () => brutForceFillword(fillword);

      expect(solve).toThrowError("Неподходящая длина для брутфорса (25 букв)");
    });

    it("should fails to generate solution (minimal)", () => {
      const fillword = generateUnsolvedFillword("ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ");
      fillword.forEach((c, idx) => {
        if (c.x === 2 || c.y === 2) {
          c.color = "black";
        }
      });

      const solve = () => brutForceFillword(fillword);

      expect(solve).toThrowError("Неподходящая длина для брутфорса (3 букв)");
    });

    it("should brutforce only two solutions", () => {
      const fillword = generateUnsolvedFillword("ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ");
      fillword.forEach((c, idx) => {
        if (c.x !== 5 && c.y !== 5) {
          c.color = "black";
        }
      });

      const solutions = brutForceFillword(fillword);

      expect(solutions).toHaveLength(2);
    });

    it("should brutforce multiple solutions", () => {
      const fillword = generateUnsolvedFillword("ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ");
      fillword.forEach((c, idx) => {
        if (c.x >= 3 || c.y === 5) {
          c.color = "black";
        }
      });

      const solutions = brutForceFillword(fillword);

      expect(solutions).toHaveLength(28);
    });
  });
});
