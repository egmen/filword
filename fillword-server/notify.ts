export function sendNotify(message: string) {
  const url = new URL(process.env.HA_NOTIFY_URL!);
  url.searchParams.set("message", message);

  return fetch(url, { method: "POST" });
}
