import { getVocabulary } from "./fillword-server/vocabulary";

const voc = getVocabulary();

export const ALL_LETTERS = "абвгдежзийклмнопрстуфхцчшщъыьэюя".split("");

export const WORD_5_VOCABULARY = voc.filter((word) => word.length === 5);

type LetterQuantityMap = Map<string, number>;

/** Количество каждой буквы по положению */
const letterQuantity = new Map<number, LetterQuantityMap>();
WORD_5_VOCABULARY.forEach((word) =>
  word.split("").forEach((letter, letterIdx) => {
    let letterMap = letterQuantity.get(letterIdx);
    if (!letterMap) {
      letterMap = new Map();
      letterQuantity.set(letterIdx, letterMap);
    }
    const letterCount = letterMap.get(letter) || 0;
    letterMap.set(letter, letterCount + 1);
  })
);

/** Частотность букв, в зависимости от положения в слове */
const letterPercents = new Map<number, LetterQuantityMap>();
letterQuantity.forEach((letterMap, letterIdx) => {
  let letterSum = 0;
  letterMap.forEach((cnt) => {
    letterSum += cnt;
  });
  const letterPercent = new Map();
  letterMap.forEach((cnt, letter) => {
    letterPercent.set(letter, Math.ceil((cnt / letterSum) * 1000) / 10);
  });
  letterPercents.set(letterIdx, letterPercent);
});

export function totalWordPercent(word: string) {
  const totalPercent = Array.from(new Set(word.split(""))).reduce(
    (prc, letter, letterIdx) => {
      const letterIdxMap = letterPercents.get(letterIdx);
      if (!letterIdxMap) {
        throw new Error(
          `Для ${
            letterIdx + 1
          } буквы в слове ${word} нет статистического распределения`
        );
      }
      const letterIdxPercent = letterIdxMap.get(letter);
      if (letterIdxPercent === undefined) {
        throw new Error(
          `Для буквы ${letter} на ${
            letterIdx + 1
          } месте нет статистического распределения`
        );
      }
      return prc + letterIdxPercent;
    },
    0
  );
  return Math.ceil(totalPercent * 10) / 10;
}

export function calcAbsentLetters(
  words: string[],
  existsLetters: string
): string {
  const allLetters = Array.from(
    new Set(words.map((word) => word.split("")).flat())
  );

  return allLetters
    .filter((letter) => !existsLetters.includes(letter))
    .join("");
}

export function calcExistsLetters(
  existLetters: string,
  mask: string[]
): string[] {
  const allLetters = existLetters
    .split("")
    .concat(mask)
    .filter((letter) => Boolean(letter));

  return Array.from(new Set(allLetters));
}
