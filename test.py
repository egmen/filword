import unittest
import cv2
import os
import fillword_image as fi


class TestFillwordImageMethods(unittest.TestCase):
    def test3x3(self):
        image_path = os.path.join(os.getcwd(), 'test', '3x3.png')
        img = cv2.imread(image_path)
        cnt = fi.count_squares(img)
        self.assertEqual(cnt, 9)

    def test4x4(self):
        image_path = os.path.join(os.getcwd(), 'test', '4x4.png')
        img = cv2.imread(image_path)
        cnt = fi.count_squares(img)
        self.assertEqual(cnt, 16)

    def test5x5(self):
        image_path = os.path.join(os.getcwd(), 'test', '5x5.png')
        img = cv2.imread(image_path)
        cnt = fi.count_squares(img)
        self.assertEqual(cnt, 25)

    def test7x7(self):
        image_path = os.path.join(os.getcwd(), 'test', '7x7.png')
        img = cv2.imread(image_path)
        cnt = fi.count_squares(img)
        self.assertEqual(cnt, 49)
