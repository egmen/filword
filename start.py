import pyautogui
import time
import numpy as np
import math
import sys
import fillword as f
moon = pyautogui.locateCenterOnScreen(f.moon_image, confidence=0.9)
if moon is not None:
    pyautogui.click(moon.x, moon.y)
sun = pyautogui.locateCenterOnScreen(f.sun_image, confidence=0.9)
topx = sun.x - 120
topy = sun.y - 52


recx = topx
recy = topy + 230
recw = 490
rech = recw


class MainProcess():
    already_checked = set()

    def get_fillword(self):
        img = pyautogui.screenshot(region=(recx, recy, recw, rech))
        open_cv_image = np.array(img)
        open_cv_image = open_cv_image[:, :, ::-1].copy()
        fw = f.parse_fillword(open_cv_image)
        return fw

    def rest_size(self, fw):
        return len(list(filter(lambda letter: not letter[3], fw)))

    def draw_word(self, word):
        X = 0
        Y = 1
        size = (int)(math.sqrt(len(self.fw)))
        square_size = (int)(recw / size)
        square_center = (int)(square_size / 2)
        is_pressed = False
        for letter in word:
            x = recx + square_center + letter[X] * square_size
            y = recy + square_center + letter[Y] * square_size
            pyautogui.moveTo(x, y)
            if not is_pressed:
                pyautogui.mouseDown()
        pyautogui.mouseUp()

    def show_word(self, word):
        return ''.join(x[2] for x in word)

    def print_word(self, word):
        if self.has_solution:
            return False
        word_string = self.show_word(word)
        print(word_string)
        self.draw_word(word)
        self.already_checked.add(word_string)
        time.sleep(1)
        close = pyautogui.locateCenterOnScreen(
            f.close_image, confidence=0.9)
        if close is not None:
            pyautogui.click(close.x, close.y)
            time.sleep(1)
        fwd = self.get_fillword()
        self.rest_cnt = self.rest_size(fwd)
        if self.rest_cnt != self.rest_size(self.fw):
            self.has_solution = True

    def print_words_filtered_by_vocabulary(self, words):
        for word in words:
            if self.has_solution:
                return False
            if f.is_in_vocabulary(word):
                word_string = self.show_word(word)
                if not word_string in self.already_checked:
                    self.print_word(word)

    def start(self):
        while 1:
            try:
                self.has_solution = False
                self.fw = self.get_fillword()
                self.rest_cnt = self.rest_size(self.fw)
                s3 = f.find_all_small_words(self.fw)
                # s3 = f.filter_by_start(words)
                self.print_words_filtered_by_vocabulary(s3)
                s4 = f.get_next_words(s3, self.fw)
                self.print_words_filtered_by_vocabulary(s4)
                s5 = f.get_next_words(s4, self.fw)
                self.print_words_filtered_by_vocabulary(s5)
                s6 = f.get_next_words(s5, self.fw)
                self.print_words_filtered_by_vocabulary(s6)
                s7 = f.get_next_words(s6, self.fw)
                self.print_words_filtered_by_vocabulary(s7)
                s8 = f.get_next_words(s7, self.fw)
                self.print_words_filtered_by_vocabulary(s8)
                s9 = f.get_next_words(s8, self.fw)
                self.print_words_filtered_by_vocabulary(s9)
                s10 = f.get_next_words(s9, self.fw)
                self.print_words_filtered_by_vocabulary(s10)
                s11 = f.get_next_words(s10, self.fw)
                self.print_words_filtered_by_vocabulary(s11)
                s12 = f.get_next_words(s11, self.fw)
                self.print_words_filtered_by_vocabulary(s12)
                if not self.has_solution:
                    print('Отгадываем брутфорсом')
                    for word in s12 + s11 + s10 + s9 + s8 + s7 + s6 + s5 + s4 + s3:
                        self.print_word(word)
                if self.rest_cnt == 0:
                    print('Новый филворд')
                    self.already_checked.clear()
                    time.sleep(10)
                else:
                    print('Продолжаем отгадывать')
                    time.sleep(1)
            except Exception as e:
                print('Неизвестная ошибка')
                print(e)
                self.already_checked.clear()
                time.sleep(10)


proc = MainProcess()
proc.start()
